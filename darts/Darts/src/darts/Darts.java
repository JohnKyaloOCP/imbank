/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package darts;

import java.util.Scanner;

/**
 *
 * @author john.kyalo
 */
public class Darts {

    public static void main(String[] args) {
        
        
        Scanner sn = new Scanner(System.in);
        System.out.println("*****Lets play dart*****");
        while (true) {
            System.out.println("Kindly feed me Integers only ..right?cool :)");
            System.out.println("Enter x cordinate:");
            int x = sn.nextInt();
            System.out.println("Enter y cordinate:");
            int y = sn.nextInt();
            int score = score(x, y);
            System.out.println("Point earned in attempt::=" + score+"\n");
            System.out.println("Enter 1 Continue OR 2 to Exit");           
            int playAgain = sn.nextInt();
            if (playAgain == 1) {
            } else {
                System.out.println("Bye!");
                System.exit(0);
            }
        }

    }

    public static int score(int x, int y) {
        double shotlength = Math.sqrt(Math.pow((double) x, 2) + Math.pow((double) y, 2));
        if (shotlength <= 1.0) {
            //inner circle
            return 10;
        } else if (shotlength <= 5.0) {
            //middle circle
            return 5;
        } else if (shotlength <= 10.0) {
            //outer circle
            return 1;
        } else {
            //outside target
            return 0;
        }

    }
}
