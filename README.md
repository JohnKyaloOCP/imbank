# Darts-Question One

This is for Question 1.To run the file,import the project under the `imbank/darts` folder using your IDE(Netbeans,Eclipse..),there is the Darts project with 1 class ```Darts.java```.Then run it.It is a console app which is interactive in playing the game.

# Heist-Question Two

Import the project using your IDE under folder `imbank/heist/TheHeist`.There is a main class ```theheist.TheHeist.java```.The project uses 1 dependency(org.json.jar) which is required on the class path.Its available under folder (<location_of_heist_on_your_machine>/heist/TheHeist/lib).For the most part,it will be set up if running the project with an IDE.

```bash
-to run the program,edit the 'String items ' variable in the main method 
and set a value for the ' int knapsackCapacity' variable 
in order to get the value of the maximum loot.
```

## Got(Game of Thrones)-Question Three
-Its under `imbank/got` folder.This is a SpringBoot &Thymeleaf web app.Run the project main class ```GotApplication.java``` using your IDE.Once it is running,you can access the index at ```http://localhost:8092/```.Links to Characters,House and Books will be displayed for you to interact with the application.

## IOU-Question four
This is a SpringBoot app which uses Mongo DB  for data storage.To use the app,please have Mongo DB installed in your environment,the app uses no authentication to connect to the database.Create a database `IOUStore` and a collection name `User`.You can use a tool like Mongo DB compass for this.
Start the application by running main class `IouApplication.java`.It starts at `http://localhost:8091/app`.The various APIS can then be accessed.For ease of running the app,you can clone the below POSTMAN collection which I have prepared and contains all the methods required in the project.
```
https://www.getpostman.com/collections/e01070e76077c5818d23
```
#### Descriptions

1.Create User-sample body included in the collection-Just edit the name and POST to create the User.A response body with the created User is returned.

2.Get User-Uses the user id (returned as a string in call no 1 above).Use it as a path variable to GET data of the particular user

3.Get All Users-a GET method to return all users

4.Delete User-uses a path variable parameter which is a string representing the id of the User to delete

5.Create IOU-POST method .Sample body in the collection.To simplify the inputs,the `lendername` and `borrowername` parameters are the names of the Users.

##### Thank you for reading this document.
