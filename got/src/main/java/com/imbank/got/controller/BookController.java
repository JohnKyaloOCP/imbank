/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imbank.got.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import com.imbank.got.model.Book;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author john.kyalo
 */
@Controller
@RequestMapping("/books")
@Slf4j
public class BookController {

    @Autowired
    RestTemplate restTemplate;

    //bk(/bk) was added to explicitly differentiate findBookById and findAllBooks Methods request mappings.
    //This is because the book id parameter is not returned as a unique field but part of the url.
    //Otherwise,I would have used a PathVariable of {id} to fetch a specific book 
    //inorder to make the method conformant to programming standards
    @GetMapping(value = "/bk", produces = MediaType.APPLICATION_JSON_VALUE)
    public String findBookById(@RequestParam(name = "q") String url, Model model) {
        ResponseEntity<Book> response = restTemplate.getForEntity(url, Book.class);
        log.info("Returned book={}", response.getBody());
        model.addAttribute("book", response.getBody());
        return "book_view";
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public String findAllBooks(Model model) {
        ResponseEntity<Book[]> response = restTemplate.getForEntity("https://www.anapioficeandfire.com/api/books?page={pgno}&pageSize={pgsize}", Book[].class, "1", "50");
        log.info("All book={}", response.getBody());
        model.addAttribute("books", response.getBody());
        return "books";
    }
}
