/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imbank.got.controller;

import com.imbank.got.model.Character;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author john.kyalo
 */
@Controller
@RequestMapping("/characters")
@Slf4j
public class CharacterController {

    @Autowired
    RestTemplate restTemplate;
    //ch(/ch) was added to explicitly differentiate findCharacterById and findAllCharacters Methods request mappings.
    //This is because the character id parameter is not returned as a unique field but part of the url.
    //Otherwise,I would have used a PathVariable of {id} to fetch a specific character 
    //inorder to make the method conformant to programming standards

    @GetMapping(value = "/ch", produces = MediaType.APPLICATION_JSON_VALUE)
    public String findCharacterById(@RequestParam(name = "q") String url, Model model) {
        ResponseEntity<Character> response = restTemplate.getForEntity(url, Character.class);
        log.info("Returned character={}", response.getBody());
        model.addAttribute("character", response.getBody());
        return "character_view";
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public String findAllCharacters(Model model) {
        ResponseEntity<Character[]> response = restTemplate.getForEntity("https://anapioficeandfire.com/api/characters?page={pgno}&pageSize={pgsize}", Character[].class, "1", "50");
        log.info("All characters={}", response.getBody());
        model.addAttribute("characters", response.getBody());
        return "characters";
    }
}
