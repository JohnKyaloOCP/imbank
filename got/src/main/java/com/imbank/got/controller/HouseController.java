/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imbank.got.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import com.imbank.got.model.House;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author john.kyalo
 */
@Controller
@RequestMapping("/houses")
@Slf4j
public class HouseController {

    @Autowired
    RestTemplate restTemplate;
    
    //hse(/hse) was added to explicitly differentiate findHouseById and findAllHouses Methods request mappings.
    //This is because the house id parameter is not returned as a unique field but part of the url.
    //Otherwise,I would have used a PathVariable of {id} to fetch a specific house
    //inorder to make the method conformant to programming standards
    @GetMapping(value = "/hse", produces = MediaType.APPLICATION_JSON_VALUE)
    public String findHouseById(@RequestParam(name = "q") String url, Model model) {
        //ResponseEntity<House> response = restTemplate.getForEntity("https://www.anapioficeandfire.com/api/houses/{id}" , House.class,id);
        ResponseEntity<House> response = restTemplate.getForEntity(url, House.class);
        log.info("Returned house={}", response.getBody());
        model.addAttribute("house", response.getBody());
        return "house_view";
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public String findAllHouses(Model model) {
        ResponseEntity<House[]> response = restTemplate.getForEntity("https://www.anapioficeandfire.com/api/houses?page={pgno}&pageSize={pgsize}", House[].class, "1", "50");
        log.info("All houses={}", response.getBody());
        model.addAttribute("houses", response.getBody());
        return "houses";
    }

}
