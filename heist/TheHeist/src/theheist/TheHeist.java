/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package theheist;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author john.kyalo ..This class uses an external library for JSON processing in the lib folder.Dependency already included as org.json.jar
 */
public class TheHeist {

    public static void main(String... args) {
         //replace the String items value with a valid JSON string depicting items in the apartment.

       // String items = "[ { \"weight\": 5, \"value\": 10 }, { \"weight\": 4, \"value\": 40 }, { \"weight\": 6, \"value\": 30 }, { \"weight\": 4, \"value\": 50 } ]";
        String items = "[ { \"weight\": 5, \"value\": 80 }, { \"weight\": 2, \"value\": 40 }, { \"weight\": 2, \"value\": 62 }, { \"weight\": 4, \"value\": 50 } , { \"weight\": 4, \"value\": 50 }, { \"weight\": 4, \"value\": 50 }, { \"weight\": 1, \"value\": 53 }, { \"weight\": 2, \"value\": 50 }]";
        int knapsackCapacity = 10;
        try {
            int totalValue = solution(items, knapsackCapacity);
            System.out.println("TotalValue::=>" + totalValue);
        } catch (Exception ex) {
            Logger.getLogger(TheHeist.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static int solution(String items, int knapsackcapacity) throws Exception {
        List<Item> bagItems = new CopyOnWriteArrayList<>();
        try {
            JSONArray array = new JSONArray(items);
            for (int i = 0; i < array.length(); i++) {
                JSONObject item = array.getJSONObject(i);
                bagItems.add(new Item(item.getInt("weight"), item.getInt("value")));
            }
            //System.out.println("Before sort:"+bagItems);
            //sort items in order of value with high value ,less weight items with preference
            Collections.sort(bagItems, new ItemComparator());
            // System.out.println("After sort:"+bagItems);
            //  List<Item> pickedItems=new ArrayList<>();
            int totalValue = 0;
            int totalWeight = 0;
            for (Item item : bagItems) {
                totalWeight += item.getWeight();
                totalValue += item.getValue();
                if (totalWeight > knapsackcapacity) {
                    //if traversed item overfills our knapsack,remove it from our list of the heist
                    bagItems.remove(item);
                    totalValue -= item.getValue();
                    totalWeight -= item.getWeight();
                }
            }
            System.out.println("TotalValue=" + totalValue + ",knapsack weight=" + totalWeight);
            System.out.println("Chosen items in knapsack=" + bagItems);
            return totalValue;
        } catch (JSONException ex) {
            Logger.getLogger(TheHeist.class.getName()).log(Level.SEVERE, null, ex);
            throw new Exception("Error procesing the heist:" + ex.getMessage());
        }

    }

}
//pojo for items 

class Item {

    @Override
    public String toString() {
        return "Item{" + "weight=" + weight + ", value=" + value + '}';
    }

    public Item() {
    }

    public Item(int weight, int value) {
        this.weight = weight;
        this.value = value;
    }

    /**
     * @return the weight
     */
    public int getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(int weight) {
        this.weight = weight;
    }

    /**
     * @return the value
     */
    public int getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(int value) {
        this.value = value;
    }
    private int weight;
    private int value;
}
//comparator to sort valuable items 

class ItemComparator implements Comparator<Item> {

    @Override
    public int compare(Item item1, Item item2) {
        if (item2.getValue() == item1.getValue()) {
            //for items with the same value but one is heavier than the other one,it will be preferred to rank the ligher item higher
            if (item2.getWeight() > item1.getWeight()) {
                return 1;
            }
            return 0;
        } else if (item2.getValue() > item1.getValue()) {
            return 1;
        } else {
            return -1;
        }

    }
}
