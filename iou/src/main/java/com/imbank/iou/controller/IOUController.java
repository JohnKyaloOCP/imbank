/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imbank.iou.controller;

import com.imbank.iou.model.IOU;
import com.imbank.iou.service.IOUService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author john.kyalo
 */
@RestController
@RequestMapping("/iou")
@Slf4j
public class IOUController {

    @Autowired
    IOUService iOUService;

    @PostMapping
    public ResponseEntity<?> createIou(@RequestBody IOU iou) {
        try {
            log.info("Creating iou ={}", iou);
            String response = iOUService.createIOU(iou);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            log.error("Error creating iou={}", e);
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
