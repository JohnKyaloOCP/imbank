/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imbank.iou.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.imbank.iou.model.User;
import com.imbank.iou.repository.UserRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 * @author john.kyalo
 */
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    @Autowired
    UserRepository userRepository;

    @PostMapping
    public ResponseEntity<?> createUser(@RequestBody User user) {
        log.info("Saving user ={}", user);
        userRepository.save(user);
        return ResponseEntity.ok(user);
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<?> getUserById(@PathVariable String id) {
        log.info("Get  user id ={}", id);
        Optional<User> user = userRepository.findById(id);
        return ResponseEntity.ok(user.isPresent() ? user : "User with supplied id does not exist");
    }

    @GetMapping
    public ResponseEntity<?> getAllUsers(@RequestBody User user) {
        log.info("Get all users");

        return ResponseEntity.ok(userRepository.findAll(Sort.by(Sort.Direction.ASC, "name")));
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity<?> deleteUser(@PathVariable String id) {
        log.info("Deleting user id ={}", id);
        userRepository.deleteById(id);
        return ResponseEntity.ok("User deleted");
    }

    
}
