/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imbank.iou.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *
 * @author john.kyalo
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class IOU {

    private String lendername;
    private String borrowername;
    private Double amount;
}
