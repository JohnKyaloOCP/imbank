/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imbank.iou.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import java.io.IOException;

/**
 *
 * @author john.kyalo
 */
public class OwedBySerializer extends StdSerializer<OwedBy> {

    public OwedBySerializer() {
        this(null);
    }

    public OwedBySerializer(Class<OwedBy> t) {
        super(t);
    }

    @Override
    public void serialize(OwedBy t, JsonGenerator jg, SerializerProvider sp) throws IOException {
        jg.writeStartObject();
        jg.writeStringField("name", t.getUser().getName());
        jg.writeNumberField("amount", t.getAmount());
        jg.writeEndObject();
    }

}
