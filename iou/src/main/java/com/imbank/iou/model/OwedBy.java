/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imbank.iou.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *
 * @author john.kyalo
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonSerialize(using = OwedBySerializer.class)
public class OwedBy {

    private Double amount;
    private User user;

}
