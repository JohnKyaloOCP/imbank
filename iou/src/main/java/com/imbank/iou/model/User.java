/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imbank.iou.model;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author john.kyalo
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Document(collection = "User")
public class User {

    @Id

    private String id;
    private String name;

    private List<Owes> owes = new ArrayList<>();

    private List<OwedBy> owed_by = new ArrayList<>();

    private Double balance = 0.0;
}
