/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imbank.iou.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.imbank.iou.model.User;
import java.util.Optional;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author john.kyalo
 */
public interface UserRepository extends MongoRepository<User, String> {

    public Optional<User> findByName(String name);

    @Query("DELETE FROM User u WHERE u.id = :id")
    public void deleteUser(@Param("id") String id);

}
