/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imbank.iou.service;

import com.imbank.iou.model.IOU;
import com.imbank.iou.model.OwedBy;
import com.imbank.iou.model.Owes;
import com.imbank.iou.model.User;
import com.imbank.iou.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author john.kyalo
 */
@Service
@Slf4j
public class IOUService {

    @Autowired
    UserRepository userRepository;

    public String createIOU(IOU iou) {
        try {
           
            User lender = userRepository.findByName(iou.getLendername()).get();
            User borrower = userRepository.findByName(iou.getBorrowername()).get();
            lender.getOwes().add(new Owes(iou.getAmount(), userRepository.findById(borrower.getId()).get()));
            borrower.getOwed_by().add(new OwedBy(iou.getAmount(), userRepository.findById(lender.getId()).get()));
            //borrower balance update
            Double lenderOwesTotal = 0.0;
            Double lenderOwedTotal = 0.0;
            Double borrowerOwesTotal = 0.0;
            Double borrowerOwedTotal = 0.0;
            //lender balance update
            for (Owes amt : lender.getOwes()) {
                lenderOwesTotal += amt.getAmount();
            }
            for (OwedBy amt : lender.getOwed_by()) {
                lenderOwedTotal += amt.getAmount();
            }
            lender.setBalance(lenderOwesTotal - lenderOwedTotal);
            //borrower balance update
            for (Owes amt : borrower.getOwes()) {
                borrowerOwesTotal += amt.getAmount();
            }
            for (OwedBy amt : borrower.getOwed_by()) {
                borrowerOwedTotal += amt.getAmount();
            }
            borrower.setBalance(borrowerOwesTotal - borrowerOwedTotal);
            userRepository.save(lender);
            userRepository.save(borrower);
            return "Iou added";
        } catch (Exception e) {
            return "An error occured:" + e.getMessage();
        }
    }
}
